# Use OSRF ROS image
FROM ros:melodic-ros-core

# Step 1 - install colcon
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu `lsb_release -cs` main" > /etc/apt/sources.list.d/ros1-latest.list' 

RUN apt-get update && apt-get install -y \
    python3-colcon-common-extensions \
    python-pip \
    && rm -rf /var/lib/apt/lists/*

# Step 2 - install vcstool
RUN pip install vcstool

# Step 3 - setup default ROS workspace.
RUN mkdir -p /ros_ws/src
